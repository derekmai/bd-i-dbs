package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"
	_ "github.com/lib/pq"
	"github.com/urfave/cli"
	"strconv"
	"encoding/json"
	bolt "github.com/coreos/bbolt"
)

type Tarjeta struct
{
	NroTarjeta, Codseguridad, Estado, Validadesde, Validahasta,Limitecompra string
	Nrocliente int
}

type Cliente struct
{
	Nrocliente int
	Nombre, Apellido, Domicilio, Telefono string
}


type rechazo struct
{
	nrorechazo, nrocomercio int
	nrotarjeta, motivo string 
	fecha time.Time
	monto float64
}

type Compra struct
{
	Nrooperacion, Nrocomercio int
	Nrotarjeta string
	Pagado bool
	Monto float64
	Fecha time.Time
}

type alerta struct
{
	nroalerta, codalerta int 
	nrorechazo sql.NullInt64
	nrotarjeta, descripcion string
	fecha time.Time
}

type resumen struct
{
	id string
}

type Comercio struct
{
	Nrocomercio int
	Nombre, Domicilio, Codigopostal, Telefono string
}

type cabecera struct
{
	nroresumen int
	nombre, apellido, domicilio, nrotarjeta string
	desde, hasta, vence time.Time
	total float64
}

type detalle struct
{
	nroresumen, nrolinea int
	fecha time.Time
	nombrecomercio string
	monto float64
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
    // abre transacción de escritura
    tx, err := db.Begin(true)
    if err != nil {
        return err
    }
    defer tx.Rollback()

    b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

    err = b.Put(key, val)
    if err != nil {
        return err
    }

    // cierra transacción
    if err := tx.Commit(); err != nil {
        return err
    }

    return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
    var buf []byte

    // abre una transacción de lectura
    err := db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(bucketName))
        buf = b.Get(key)
        return nil
    })

    return buf, err
}

func main() {
	app := cli.NewApp()
	app.Name = "CLI TP"
	app.Usage = "Usada para aprobar la materia"

	myFlags := []cli.Flag{
		cli.StringFlag{
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "listartarjetas",
			Usage: "Lista las tarjetas",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				fmt.Println(err)
				fmt.Println(db)
				defer db.Close()	

				rows, err := db.Query(`select * from tarjeta`)
				if err != nil {
					log.Fatal(err)
				}
				
				defer rows.Close()
				var a Tarjeta
				for rows.Next() {
					if err := rows.Scan(&a.NroTarjeta, &a.Nrocliente, &a.Validadesde, &a.Validahasta, &a.Codseguridad, &a.Limitecompra, &a.Estado); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("nroCliente: %v, nroTarjeta: %v, codigo %v, limite: %v, estado: %v, desde: %v, hasta: %v\n", a.Nrocliente, a.NroTarjeta, a.Codseguridad, a.Limitecompra, a.Estado, a.Validadesde, a.Validahasta)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "listarclientes",
			Usage: "Lista los clientes",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()
				fmt.Println(err)
				fmt.Println(db)
		
				rows, err := db.Query(`select * from cliente`)
				if err != nil {
					log.Fatal(err)
				}

				defer rows.Close()
				var a Cliente
				for rows.Next() {
					if err := rows.Scan(&a.Nrocliente, &a.Nombre, &a.Apellido, &a.Domicilio, &a.Telefono); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("nroCliente: %v, nombre: %v, apellido: %v, domicilio: %v, telefono: %v \n", a.Nrocliente, a.Nombre, a.Apellido, a.Domicilio, a.Telefono)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "listarrechazos",
			Usage: "Lista los rechazos",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()
				rows, err := db.Query(`select * from rechazo`)
				if err != nil {
					log.Fatal(err)
				}

				defer rows.Close()
				var a rechazo
				for rows.Next() {
					if err := rows.Scan(&a.nrorechazo, &a.nrocomercio, &a.nrotarjeta, &a.fecha, &a.monto); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("nroRechazo: %v, nroComercio: %v, nroTarjeta: %v, monto: %v, fecha: %v \n",a.nrorechazo, a.nrocomercio, a.nrotarjeta, a.monto, a.fecha)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "listarcompras",
			Usage: "Lista las compras",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()
				rows, err := db.Query(`select * from compra`)
				if err != nil {
					log.Fatal(err)
				}

				defer rows.Close()
				var a Compra
				for rows.Next() {
					if err := rows.Scan(&a.Nrooperacion, &a.Nrocomercio, &a.Nrotarjeta, &a.Fecha, &a.Monto,&a.Pagado); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("%v %v %v %v %v %v \n",a.Nrooperacion, a.Nrocomercio, a.Nrotarjeta, a.Monto, a.Pagado, a.Fecha)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "listaralertas",
			Usage: "Lista las alertas",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()	

				rows, err := db.Query(`select * from alerta`)
				if err != nil {
					log.Fatal(err)
				}
				
				defer rows.Close()
				var a alerta
				for rows.Next() {
					if err := rows.Scan(&a.nroalerta, &a.nrotarjeta, &a.fecha, &a.nrorechazo, &a.codalerta,&a.descripcion); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("nroalerta: %v, codAlerta: %v, nroTarjeta: %v, descripcion: %v, nroRechazo: %v, fecha: %v \n", a.nroalerta, a.codalerta, a.nrotarjeta, a.descripcion, a.nrorechazo, a.fecha)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "listarresumenes",
			Usage: "Lista los resumentes",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()	

				rows, err := db.Query(`select * from cabecera`)
				if err != nil {
					log.Fatal(err)
				}
				
				defer rows.Close()
				var a cabecera
				for rows.Next() {
					if err := rows.Scan(&a.nroresumen, &a.nombre, &a.apellido, &a.domicilio, &a.nrotarjeta,&a.desde,&a.hasta,&a.vence,&a.total); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("nro: %v, nombre: %v, apellido: %v, domicilio: %v, tarjeta: %v, total: %v, desde: %v, hasta: %v, vence: %v\n", a.nroresumen, a.nombre, a.apellido, a.domicilio, a.nrotarjeta, a.total, a.desde, a.hasta, a.vence)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "listardetalles",
			Usage: "Lista los detalles de las compras",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()	

				rows, err := db.Query(`select * from detalle`)
				if err != nil {
					log.Fatal(err)
				}
				
				defer rows.Close()
				var a detalle
				for rows.Next() {
					if err := rows.Scan(&a.nroresumen, &a.nrolinea, &a.fecha, &a.nombrecomercio, &a.monto); err != nil {
						log.Fatal(err)
					}
					fmt.Printf("nro: %v, nroLinea: %v, nombreComercio: %v, monto: %v, fecha: %v\n", a.nroresumen, a.nrolinea, a.nombrecomercio, a.monto, a.fecha)
				}
				if err = rows.Err(); err != nil {
					log.Fatal(err)
				}
					return nil
				},
		},
		{
			Name:  "autorizarcompra",
			Usage: "dados los datos de un consumo, pasa por el proceso de autorizacion de compra. Parametros: numTarjeta, codigo, numComercio, montoCompra",
			Flags: myFlags,
			Action: func(c *cli.Context) error {
				numTarjeta := ""
				cod := ""
				numComercio := 0
				
				numTarjeta = c.Args().Get(0)
				cod = (c.Args().Get(1))
				numComercio, err := strconv.Atoi(c.Args().Get(2))
				if err != nil {
					log.Fatal(err)
				}
				montoCompra, err := strconv.Atoi(c.Args().Get(3))
				if err != nil {
					log.Fatal(err)
				}
				/*fmt.Println(numTarjeta)
				fmt.Println(cod)
				fmt.Println(numComercio)
				fmt.Println(montoCompra)*/
				db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
				if err != nil {
					log.Fatal(err)
				}
				defer db.Close()

				//numTarjeta char(16),cod char(4),numComercio int,montoCompra decimal(7,2) 
				stmt, err := db.Prepare("SELECT autorizarCompra(numTarjeta := $1, cod := $2, numComercio := $3, montoCompra := $4)")
				if err != nil {
					log.Fatal(err)
				}
				fmt.Println("test")
				result, err := stmt.Query(numTarjeta,cod,numComercio,montoCompra)
				var value bool
				for result.Next(){
					if err := result.Scan(&value); err != nil {
						log.Fatal(err)
					}
					resulttxt := "RECHAZADA"
					if value == true{
						resulttxt = "AUTORIZADA";
					}
					fmt.Printf("COMPRA %v \n", resulttxt)
				}
					return nil
				},
		},
		{//'4656987551234684',17,'201018','201024','668','15000','vigente';
		Name:  "generarresumen",
		Usage: "dados el numero de cliente y el mes, genera su respectivo resumen. Parametros: numCliente, numMes",
		Flags: myFlags,
		Action: func(c *cli.Context) error {
			numCliente, err := strconv.Atoi(c.Args().Get(0))
			if err != nil {
				log.Fatal(err)
			}
			numMes, err := strconv.Atoi(c.Args().Get(1))

			if err != nil {
				log.Fatal(err)
			}
			fmt.Println(numCliente)
			fmt.Println(numMes)
			db,err := sql.Open("postgres", "user=postgres host=localhost password=pass dbname=tp2 sslmode=disable")
			if err != nil {
				log.Fatal(err)
			}
			defer db.Close()

			//numTarjeta char(16),cod char(4),numComercio int,montoCompra decimal(7,2) 
			stmt, err := db.Prepare("SELECT generarResumen(numCliente := $1, numMes := $2)")
			if err != nil {
				log.Fatal(err)
			}

			result, err := stmt.Query(numCliente,numMes)
			var resumen resumen

			for result.Next(){
				if err := result.Scan(&resumen.id); err != nil {
					log.Fatal(err)
				}
				fmt.Printf("%v \n",resumen.id)
			}
				return nil
			},
	},
	{
		Name:  "guardarnosql",
		Usage: "Guarda en una base de datos pedidos en una base nosql",
		Flags: myFlags,
		Action: func(c *cli.Context) error {
			db, err := bolt.Open("tp2.db", 0600, nil)
			if err != nil {
				log.Fatal(err)
			}
			defer db.Close()
	
			
			cliente1 := Cliente{1, "Cristian", "Casais","casa1","1545787451"}
			cliente2 := Cliente{2, "Derek","Mai","casa2","1548456214"}
			cliente3 := Cliente{3, "Gerardo", "Estrada","casa3","1564987562"}

			data, err := json.Marshal(cliente1)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente1.Nrocliente)), data)
			
			data, err = json.Marshal(cliente2)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente2.Nrocliente)), data)

			data, err = json.Marshal(cliente3)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente3.Nrocliente)), data)

			resultado, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente1.Nrocliente)))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente2.Nrocliente)))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente3.Nrocliente)))
			fmt.Printf("%s\n", resultado)

			fmt.Println("")
			//TARJETAS


			tarjeta1 := Tarjeta{"4562365985421253","956","vigente", "030614", "030620", "5000",1}
			tarjeta2 := Tarjeta{"4586926532164575","156","vigente", "010111", "010119", "13000",2}
			tarjeta3 := Tarjeta{"4758956242365142","645","vigente", "201116", "201122", "9000",3}

			data, err = json.Marshal(tarjeta1)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "tarjeta", []byte(tarjeta1.NroTarjeta), data)
			
			data, err = json.Marshal(tarjeta2)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "tarjeta", []byte(tarjeta2.NroTarjeta), data)

			data, err = json.Marshal(tarjeta3)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "tarjeta", []byte(tarjeta3.NroTarjeta), data)

			resultado, err = ReadUnique(db, "tarjeta", []byte(tarjeta1.NroTarjeta))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "tarjeta", []byte(tarjeta2.NroTarjeta))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "tarjeta", []byte(tarjeta3.NroTarjeta))
			fmt.Printf("%s\n", resultado)

			fmt.Println("")

			//COMERCIOS

			comercio1 := Comercio{1, "Comercio1", "Calle1","1986","1548952645"}
			comercio2 := Comercio{2, "Comercio2","Calle2","1985","1542621572"}
			comercio3 := Comercio{3, "Gerardo", "Estrada","casa3","1564987562"}

			data, err = json.Marshal(comercio1)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio1.Nrocomercio)), data)

			data, err = json.Marshal(comercio2)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio2.Nrocomercio)), data)

			data, err = json.Marshal(comercio3)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio3.Nrocomercio)), data)

			resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio1.Nrocomercio)))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio2.Nrocomercio)))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio3.Nrocomercio)))
			fmt.Printf("%s\n", resultado)

			fmt.Println("")
			
			//COMPRAS

			compra1 := Compra{1, 2, "4562365985421253",false,123.45,time.Now()}
			compra2 := Compra{2, 3, "4586926532164575",true,456.78,time.Now()}
			compra3 := Compra{3, 1, "4758956242365142",true,768.90,time.Now()}

			data, err = json.Marshal(compra1)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "compra", []byte(strconv.Itoa(compra1.Nrooperacion)), data)

			data, err = json.Marshal(compra2)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "compra", []byte(strconv.Itoa(compra2.Nrooperacion)), data)

			data, err = json.Marshal(compra3)
			if err != nil {
				log.Fatal(err)
			}
			CreateUpdate(db, "compra", []byte(strconv.Itoa(compra3.Nrooperacion)), data)

			resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra1.Nrooperacion)))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra2.Nrooperacion)))
			fmt.Printf("%s\n", resultado)

			resultado, err = ReadUnique(db, "compra", []byte(strconv.Itoa(compra3.Nrooperacion)))
			fmt.Printf("%s\n", resultado)

			return nil;
		},
	},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
