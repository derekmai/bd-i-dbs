\c tp2

--DROP FUNCTION
--if exists generarResumen(numCliente int, int numMes int)

create function generarResumen(numCliente int ,numMes int) returns table (nrolinea int,nre int, fecha date, nombrecomercio text, monto decimal(7,2))
as $$
    declare 
        maxCabecera int;
        maxDetalle int;
        termina int;
        total decimal(7,2);
        serie int;
        bandera1 int;
        bandera2 int;
        cantTarjetas int;
    BEGIN
        if exists(select * from cabecera)  THEN
            maxCabecera:=(select max(c.nroresumen) from cabecera c);
            maxCabecera:=maxCabecera+1;
        ELSE
            maxCabecera:=(select 1);
         end if;


        CREATE LOCAL TEMP TABLE tars (id serial primary key, tarje TEXT, mina INT, ncliente INT);
        insert into tars(tarje,mina,ncliente)
        select distinct t.nrotarjeta, substring(t.nrotarjeta,16,1)::INT, numCliente
        from tarjeta t  
        join compra co
        on co.nrotarjeta=t.nrotarjeta
        where nrocliente=numCliente and date_part('month',co.fecha) = numMes;

        CREATE local temp table totales(tarje text, total decimal(8,2));
        insert into totales(tarje, total)
        select distinct(c.nrotarjeta),SUM(c.monto) from compra c group by nrotarjeta;
      
        CREATE LOCAL TEMP TABLE resultado (id int, nombre text, apellido text, domicilio text, nrotarjet char(16), desd date, hast date, vto date, tota decimal(8,2));
        
        bandera2:=(select max(r.nroresumen) from cabecera r);
        if  (bandera2 is null)THEN
            bandera2:=0;
            --raise notice 'hola1';
        end if;

        --raise notice '"%"',bandera2;

        insert into resultado(id,nombre,apellido,domicilio,nrotarjet,desd,hast,vto,tota)
        select bandera2+t.id , c.nombre, c.apellido, c.domicilio, t.tarje, to_date(to_char(ci.fechainicio,'yyyy-mm-dd'),'yyyy-mm-dd'),
        to_date(to_char(ci.fechacierre,'yyyy-mm-dd'),'yyyy-mm-dd'),to_date(to_char(ci.fechavto,'yyyy-mm-dd'),'yyyy-mm-dd'),tot.total
        from cliente c
        join tars t 
        on c.nrocliente=t.ncliente
        join cierre ci
        on ci.terminacion =t.mina and ci.mes=numMes
        join totales tot
        on tot.tarje=t.tarje
        where c.nrocliente=numCliente ;
        
        insert into cabecera(nroresumen,nombre,apellido,domicilio,nrotarjeta,desde,hasta,vence,total)
        select distinct r.id,r.nombre,r.apellido,r.domicilio,r.nrotarjet,r.desd,r.hast,r.vto,r.tota from resultado r;

        insert into detalle(nroresumen, nrolinea, fecha, nombrecomercio,monto)
        select r.id, row_number() over (PARTITION by r.id )  ,
        to_date(to_char(comp.fecha,'yyyy-mm-dd'),'yyyy-mm-dd'), co.nombre, comp.monto
        from resultado r
        join tars t
        on r.nrotarjet = t.tarje
        JOIN compra comp 
        ON comp.nrotarjeta=t.tarje 
        join comercio co
        on co.nrocomercio = comp.nrocomercio;

        return query
        select * from detalle ;
        end;
    $$ language plpgsql;
  

