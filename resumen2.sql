\c tp2

DROP FUNCTION
if exists generarResumen(numCliente int, numMes int);

create function generarResumen(numCliente int ,numMes int) returns 
--boolean
table 
--(id int,tarje TEXT,mina INT ,ncliente INT)
(id int ,  nombre text, apellido text, domicilio text, nrotarjet char(16),desd date,hast date, vto date,tota decimal(8,2))
as $$
    declare 
        maxCabecera int;
        maxDetalle int;
        termina int;
        total decimal(7,2);
        serie int;
        bandera1 int;
        bandera2 int;
        cantTarjetas int;
    BEGIN
        if exists(select * from cabecera)  THEN
            maxCabecera:=(select max(c.nroresumen) from cabecera c);
            maxCabecera:=maxCabecera+1;
        ELSE
            maxCabecera:=(select 1);
         end if;

        if exists(select * from tarjeta t where nrocliente=numCliente)THEN
            cantTarjetas:=(select count(*)from tarjeta where nrocliente=numCliente );
        end if;   



        CREATE LOCAL TEMP TABLE tars(id serial primary key,tarje TEXT,mina INT, ncliente int);
        insert into tars(tarje,mina,ncliente)
        select distinct t.nrotarjeta,substring(t.nrotarjeta,16,1)::INT,numCliente from tarjeta t  
        join compra co
        on co.nrotarjeta=t.nrotarjeta
        where nrocliente=numCliente and date_part('month',co.fecha) = numMes
        ;

        --to_date(to_char(ci.fechacierre,'yyyy-mm-dd'),'yyyy-mm-dd')
/*
        create local temp table codigo (id int, tarj text);
        bandera1:=maxCabecera-1;
        while (bandera1 < cantTarjetas)loop
            insert into codigo(id,tarj)
            select bandera1+1,t.tarje from tars t limit 1;
            bandera1:=bandera1+1;
            */
/*
            DELETE FROM tars 
            WHERE ctid IN (
            SELECT ctid
            FROM tars
            where ts<now()
            ORDER BY ts
            LIMIT 1);
*/

--        end loop;

         CREATE local temp table totales(tarje text, total decimal(8,2));
         insert into totales(tarje, total)
         select distinct(nrotarjeta),SUM(monto) from compra group by nrotarjeta;
      
        --raise notice '"%"',  select count(*)from tars;
        --raise notice '"%"', bandera;
        --id SERIAL PRIMARY KEY
        CREATE LOCAL TEMP TABLE resultado (id int, nombre text, apellido text, domicilio text
        , nrotarjet char(16),desd date, hast date, vto date,tota decimal(8,2));
        
       -- ALTER SEQUENCE resultado_id_seq RESTART WITH 1 BY 1;
       --serie:=(SELECT CURRVAL('resultado_id_seq'));
       --raise notice '"%"', serie;
       bandera2:=(select max(r.nroresumen) from cabecera r);
        if  (bandera2 is null)THEN
            bandera2:=0;
            --bandera2:=(select max(r.id) from resultado r);
            raise notice 'hola1';
--        ELSE
            --bandera2:=bandera2;
            --raise notice 'hola2';
        end if;

        raise notice '"%"',bandera2;
/*
        create local temp table ids (id serial primary key , tarj char(16))
        insert into ids(tarj)
        select distinct 
*/
        insert into resultado(id,nombre,apellido,domicilio,nrotarjet,desd,hast,vto,tota)
        select bandera2+t.id , c.nombre, c.apellido, c.domicilio, t.tarje, to_date(to_char(ci.fechainicio,'yyyy-mm-dd'),'yyyy-mm-dd'),
            to_date(to_char(ci.fechacierre,'yyyy-mm-dd'),'yyyy-mm-dd'),to_date(to_char(ci.fechavto,'yyyy-mm-dd'),'yyyy-mm-dd'),
        tot.total
        from cliente c
        join tars t on c.nrocliente=t.ncliente
        join cierre ci
        on ci.terminacion =t.mina and ci.mes=numMes
        join totales tot
        on tot.tarje=t.tarje
        --join codigo cod
        --on cod.tarj=t.tarje
        where c.nrocliente=numCliente ;
        --and t.tarje in(select distinct(tar) from codigo); 

        
        --bandera1:=maxCabecera;
        --while (bandera1 < cantTarjetas)loop

        --update resultado 
        --set id = maxCabecera + id;

        --end loop;

        insert into cabecera(nroresumen,nombre,apellido,domicilio,nrotarjeta,desde,hasta,vence,total)
        select distinct r.id,r.nombre,r.apellido,r.domicilio,r.nrotarjet,r.desd,r.hast,r.vto,r.tota from resultado r;

         return 
         --true;
         query
        select * from cabecera;
       --select * from tars;

        --termina:=(SELECT substring(nrotarjeta,16,1) FROM tarjeta where nrocliente=numCliente);
        
        --raise notice '"%"',  termina;

         end;
    $$ language plpgsql;
  
         /*
cabecera ( 
nroresumen int, 
nombre text, 
apellido text, 
domicilio text, 
nrotarjeta char(16), 
desde date, 
hasta date, 
vence date, 
total decimal(8,2) 
); 

detalle ( 
nroresumen int,
nrolinea int, 
fecha date, 
nombrecomercio text, 
monto decimal(7,2) 
); 
*/
