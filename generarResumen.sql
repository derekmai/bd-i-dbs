--generación del resumen el trabajo práctico deberá contener la lógica que reciba como parámetros 
--el número de cliente, y el periodo del año, y que guarde en las tablas que corresponda los datos del resumen con la siguiente información  
--nombre y apellido, dirección, número de tarjeta, periodo del resumen, fecha de vencimiento, todas las compras del periodo, y total a pagar
/*
cabecera ( 
nroresumen int, 
nombre text, 
apellido text, 
domicilio text, 
nrotarjeta char(16), 
desde date, 
hasta date, 
vence date, 
total decimal(8,2) 
); 
detalle ( 
nroresumen int,
nrolinea int, 
fecha date, 
nombrecomercio text, 
monto decimal(7,2) 
); 
*/

create or replace procedure generarResumen(cliente int, desde date, hasta date, vence date) return boolean$$
begin
	declare nomCliente text;
	declare apeCliente text;
	declare domCliente text;
	declare numTarjeta char(16);
	declare ultResumen int;
	declare fin boolean;
	
	declare fechaCom date;
	declare montoCom decimal(7,2);
	declare nombreCom text;
	declare numLinea int;
	declare totalCompras decimal(8,2);
	
	-- levanta datos personales del cliente
	select cl.nombre, cl.apellido, cl.domicilio, t.nrotarjeta into nomCliente, apeCliente, domCliente, numTarjeta 
	from cliente cl inner join tarjeta t on t.nrocliente = cl.nrocliente where cl.nrocliente = cliente;
	
	-- determina el código incremental de la tabla resumen
	if not exists (select max(nroresumen)+1 into ultResumen from cabecera) then
		set ultResumen = 1
	end if
	-- ingresa los datos en la cabecera (el total es 0 por ahora)
	insert into cabecera values (ultresumen, nomCliente, apeCliente, numTarjeta, desde, hasta, vence, 0);
	-- abre un cursor con el detalle de compras
	declare buscar_compras cursor for 
		select co.fecha, co.monto, c.nombre 
		from compra co inner join comercio cm on cn.nrocomercio = co.nrocomercio 
		where co.fecha between desde and hasta and co.nroTarjeta = numTarjeta;
	
	-- declara un handler para detectar fin de datos
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = true;

	open cursor buscar_compras;

	set fin = false;
	set numLinea = 0;
	set totalCompras = 0;
	-- itera en la consulta
	fetch buscar_compras into fechaCom, montoCom, nombreCom
	while not fin
		set numLinea = numLinea + 1;
		-- inserta un registro de detalle
		insert into detalle values(ultResumen, numLinea, fechaCom, montoCom, nombreCom);
		-- acumula el importe
		set totalCompras = totalCompras + montoCom;
		fetch buscar_compras into fechaCom, montoCom, nombreCom
	loop;
	-- finalmente actualiza el total
	update cabecera set total = totalCompras where nroresumen = ultResumen;
end$$
language plpgsql;