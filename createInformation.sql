------------CLIENTES--------------
--drop database
--if exists tp2;

--create 
--database tp2;

\c tp2

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 1,'Cristian','Casais','casa1','1545787451';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 2,'Derek','Mai','casa2','1548456214';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 3,'Gerardo','Estrada','casa3','1564987562';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 4,'Cinthia','Cardenas','casa4','1547851364';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 5,'Christian','Amato','casa5','1594759123';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 6,'Daniel','Niño','casa6','1547851452';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 7,'Enrique','Sanchez','casa7','1564957562';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 8,'Gian','Sansur','casa8','1541267985';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 9,'Giuliano','Trujillo','casa9','1546976413';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 10,'Javier','Carabajal','casa10','1547895461';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 11,'Juan','Carrillo','casa11','1546978562';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 12,'Luis','Leon','casa12','15649847585';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 13,'Alberr','Arias','casa13','1564987585';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 14,'Claudio','Maturana','casa14','1564957585';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 15,'Emanuel','Santillan','casa15','1564987856';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 16,'Emerson','Ananias','casa16','1564646478';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 17,'Jasna','Fuentes','casa17','1532322346';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 18,'Javier','Castro','casa18','1564785152';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 19,'Mariana','Guerra','casa19','1564649875';

insert into cliente(nrocliente,nombre,apellido,domicilio,telefono)
select 20,'Matias','Prestia','casa20','1511112244';

------------TARJETAS----------------
insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4659757861542985',1,'201116','201122','156','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4578459516458542',1,'201117','201123','685','15000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4685957432651425',2,'201114','201120','957','16000','suspendida';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4857659845123562',2,'211017','211023','845','20000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4585612535624578',3,'100514','100520','742','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4562365985421253',4,'030614','030620','956','5000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4586926532164575',5,'010111','010119','156','13000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4758956242365142',6,'201116','201122','645','9000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4362514275986524',7,'050606','050626','147','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4958765842515263',8,'180912','180917','958','25000','anulada';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4587945625364251',9,'211216','211222','645','7000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4141525263537849',10,'150314','150320','741','15500','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4651524398758462',11,'160115','150121','845','16500','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4695857462546128',12,'220616','220622','654','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4659857464521345',13,'300714','300720','689','30000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4987758465124352',14,'311212','311218','861','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4658759524984616',15,'151015','151021','445','17000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4656454512324875',16,'170612','170619','654','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4656987551234684',17,'201018','201024','668','15000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4567849515623245',18,'150914','150920','888','10000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4695781523654585',19,'130115','130121','111','25000','vigente';

insert into tarjeta(nrotarjeta,nrocliente,validadesde,validahasta,codseguridad,limitecompra,estado)
select '4512321547859562',20,'150316','150622','885','10000','vigente';


---------COMERCIO---------

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 1,'Comercio1','Calle1','1986','1548952645';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 2,'Comercio2','Calle2','1985','1542621572';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 3,'Comercio3','Calle3','1532','44562827';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 4,'Comercio4','Calle4','1645','44563285';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 5,'Comercio5','Calle5','1852','1542518574';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 6,'Comercio6','Calle6','1752','1562459586';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 7,'Comercio7','Calle7','1532','44560252';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 8,'Comercio8','Calle8','1642','1595969562';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 9,'Comercio9','Calle9','1785','1542625392';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 10,'Comercio10','Calle10','1456','44569251';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 11,'Comercio11','Calle11','1262','1574857492';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 12,'Comercio12','Calle12','1613','1564529262';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 13,'Comercio13','Calle13','1652','44561892';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 14,'Comercio14','Calle14','1694','44560362';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 15,'Comercio15','Calle15','1452','1584958495';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 16,'Comercio16','Calle16','1852','44560618';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 17,'Comercio17','Calle17','1613','44561818';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 18,'Comercio18','Calle8','1752','1584958474';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 19,'Comercio19','Calle19','1645','1562514162';

insert into comercio(nrocomercio,nombre,domicilio,codigopostal,telefono)
select 20,'Comercio20','Calle20','1623','44569192';

----------------CIERRE------------------

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,0,'2018-01-01','2018-01-30','2018-02-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,1,'2018-01-01','2018-01-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,2,'2018-01-02','2018-01-29','2018-02-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,3,'2018-01-01','2018-01-29','2018-02-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,4,'2018-01-03','2018-01-31','2018-02-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,5,'2018-01-02','2018-01-30','2018-02-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,6,'2018-01-01','2018-01-29','2018-02-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,7,'2018-01-03','2018-02-01','2018-02-07';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,8,'2018-01-02','2018-01-29','2018-02-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,1,9,'2018-01-03','2018-01-30','2018-02-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,0,'2018-02-01','2018-03-02','2018-03-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,1,'2018-02-02','2018-02-28','2018-03-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,2,'2018-02-01','2018-02-27','2018-03-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,3,'2018-02-03','2018-02-26','2018-03-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,4,'2018-02-04','2018-02-28','2018-03-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,5,'2018-02-01','2018-02-28','2018-03-02';


insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,6,'2018-02-02','2018-03-01','2018-03-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,7,'2018-02-01','2018-02-28','2018-03-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,8,'2018-02-04','2018-02-27','2018-03-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,2,9,'2018-02-03','2018-03-01','2018-03-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,0,'2018-03-01','2018-03-31','2018-04-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,1,'2018-03-02','2018-03-30','2018-04-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,2,'2018-03-02','2018-03-31','2018-04-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,3,'2018-03-01','2018-03-30','2018-04-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,4,'2018-03-03','2018-03-29','2018-04-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,5,'2018-03-04','2018-03-28','2018-04-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,6,'2018-03-05','2018-03-29','2018-04-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,7,'2018-03-01','2018-03-31','2018-04-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,8,'2018-03-02','2018-03-30','2018-04-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,3,9,'2018-03-03','2018-03-31','2018-04-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,0,'2018-04-04','2018-04-30','2018-05-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,1,'2018-04-01','2018-04-29','2018-05-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,2,'2018-04-02','2018-04-30','2018-05-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,3,'2018-04-03','2018-04-28','2018-05-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,4,'2018-04-01','2018-04-28','2018-05-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,5,'2018-04-02','2018-04-29','2018-05-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,6,'2018-04-01','2018-04-29','2018-05-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,7,'2018-04-03','2018-04-30','2018-05-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,8,'2018-04-04','2018-04-29','2018-05-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,4,9,'2018-04-05','2018-04-30','2018-05-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,0,'2018-05-06','2018-05-31','2018-06-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,1,'2018-05-05','2018-05-31','2018-06-04';
<<<<<<< HEAD
=======

>>>>>>> 8a184598b5bd9b9017629565847d15954f208e10

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,2,'2018-05-04','2018-05-29','2018-06-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,3,'2018-05-03','2018-05-29','2018-06-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,4,'2018-05-02','2018-05-30','2018-06-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,5,'2018-05-01','2018-05-30','2018-06-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,6,'2018-05-02','2018-05-29','2018-06-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,7,'2018-05-03','2018-05-31','2018-06-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,8,'2018-05-04','2018-05-30','2018-06-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,5,9,'2018-05-05','2018-05-30','2018-06-06';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,0,'2018-06-01','2018-06-30','2018-07-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,1,'2018-06-02','2018-06-29','2018-07-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,2,'2018-06-05','2018-06-28','2018-07-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,3,'2018-06-02','2018-06-30','2018-07-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,4,'2018-06-03','2018-06-30','2018-07-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,5,'2018-06-04','2018-06-29','2018-07-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,6,'2018-06-01','2018-06-29','2018-07-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,7,'2018-06-03','2018-06-28','2018-07-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,8,'2018-06-02','2018-06-28','2018-07-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,6,9,'2018-06-01','2018-06-30','2018-07-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,0,'2018-07-03','2018-07-31','2018-08-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,1,'2018-07-04','2018-07-30','2018-08-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,2,'2018-07-01','2018-07-28','2018-08-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,3,'2018-07-02','2018-07-29','2018-08-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,4,'2018-07-03','2018-07-30','2018-08-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,5,'2018-07-04','2018-07-31','2018-08-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,6,'2018-07-02','2018-07-29','2018-08-02';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,7,'2018-07-01','2018-07-30','2018-08-03';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,8,'2018-07-02','2018-07-31','2018-08-04';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,7,9,'2018-07-03','2018-07-29','2018-08-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,0,'2018-08-04','2018-08-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,1,'2018-08-04','2018-08-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,2,'2018-08-05','2018-08-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,3,'2018-08-04','2018-08-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,4,'2018-08-03','2018-08-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,5,'2018-08-02','2018-08-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,6,'2018-08-01','2018-08-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,7,'2018-08-02','2018-08-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,8,'2018-08-01','2018-08-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,8,9,'2018-08-02','2018-08-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,0,'2018-09-03','2018-09-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,1,'2018-09-01','2018-09-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,2,'2018-09-01','2018-09-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,3,'2018-09-02','2018-09-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,4,'2018-09-03','2018-09-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,5,'2018-09-04','2018-09-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,6,'2018-09-05','2018-09-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,7,'2018-09-03','2018-09-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,8,'2018-09-02','2018-09-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,9,9,'2018-09-01','2018-09-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,0,'2018-10-01','2018-10-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,1,'2018-10-03','2018-10-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,2,'2018-10-02','2018-10-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,3,'2018-10-01','2018-10-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,4,'2018-10-03','2018-10-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,5,'2018-10-04','2018-10-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,6,'2018-10-05','2018-10-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,7,'2018-10-04','2018-10-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,8,'2018-10-02','2018-10-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,10,9,'2018-10-01','2018-10-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,0,'2018-11-03','2018-11-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,1,'2018-11-04','2018-11-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,2,'2018-11-02','2018-11-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,3,'2018-11-01','2018-11-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,4,'2018-11-02','2018-11-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,5,'2018-11-03','2018-11-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,6,'2018-11-04','2018-11-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,7,'2018-11-05','2018-11-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,8,'2018-11-01','2018-11-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,11,9,'2018-11-02','2018-11-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,0,'2018-12-03','2018-12-28','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,1,'2018-12-01','2018-12-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,2,'2018-12-04','2018-12-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,3,'2018-12-05','2018-12-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,4,'2018-12-03','2018-12-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,5,'2018-12-02','2018-12-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,6,'2018-12-01','2018-12-29','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,7,'2018-12-02','2018-12-30','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,8,'2018-12-03','2018-12-31','2018-02-05';

insert into cierre(año,mes,terminacion,fechainicio,fechacierre,fechavto)
select  2018,12,9,'2018-12-01','2018-12-29','2018-02-05';

*/
