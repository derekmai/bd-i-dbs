﻿/*
comercio( 
nrocomercio:int, 
nombre:text, 
domicilio:text, 
codigopostal:char(8), 
telefono:char(12) ) 

compra( 
nrooperacion:int, 
nrotarjeta:char(16), 
nrocomercio:int, 
fecha:timestamp, 
monto:decimal(7,2), 
pagado:boolean )

Si una tarjeta registra dos compras en un lapso menor de un minuto en comercios distintos ubicados en el mismo código postal.− 
Si una tarjeta registra dos compras en un lapso menor de5minutos en comercios con diferentes códigos postales
*/
-- funciones para los trigger
\c tp2

DROP FUNCTION
if exists alertarCompra() CASCADE;
DROP FUNCTION
if exists alertarRechazo() CASCADE;


create function alertarCompra() returns trigger as $$

	DECLARE
		postal char(8);
		cant int;
		numAlerta int;
	BEGIN
		
		select codigopostal into postal from comercio where nrocomercio = new.nrocomercio;
		
		select count(*) into cant from compra co inner join comercio cm on co.nrocomercio = cm.nrocomercio
		where cm.codigopostal = postal and date(co.fecha) = date(new.fecha) 
		and to_date(to_char(new.fecha, 'HH:MM:SS'),'HH:MM:SS') - to_date(to_char(co.fecha, 'HH:MM:SS'),'HH:MM:SS') < 1;
		

		if cant > 1 then
			if exists(select * from alerta)  THEN
				numAlerta:=(select max(a.nroalerta) from alerta a);
				numAlerta:=numAlerta+1;
        	ELSE
           		numAlerta:=(select 1);
         	end if;
			
			insert into alerta values (numAlerta, new.nrotarjeta, new.fecha, null, 1, 'compras en un lapso menor de un minuto igual Cod. Postal');
		end if;

		
		
		cant = 0;

		select count(*) into cant from compra co inner join comercio cm on co.nrocomercio = cm.nrocomercio 
		where cm.codigopostal <> postal and date(co.fecha) = date(new.fecha) 
		and to_date(to_char(new.fecha, 'HH:MM:SS'),'HH:MM:SS') - to_date(to_char(co.fecha, 'HH:MM:SS'),'HH:MM:SS') < 5;
		
		if cant > 1 then
			if exists(select * from alerta)  THEN
				numAlerta:=(select max(a.nroalerta) from alerta a);
				numAlerta:=numAlerta+1;
        	ELSE
           		numAlerta:=(select 1);
         	end if;
			insert into alerta values (numAlerta, new.nrotarjeta, new.fecha, null, 5, 'compras lapso menor de 5 minutos,diferentes Cod. Postal');
		end if;
		return new;
end;

$$ language plpgsql;

/*
alerta ( 
nroalerta:int, 
nrotarjeta:char(16), 
fecha:timestamp, 
nrorechazo:int, 
codalerta:int, --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite 
descripcion:text );

−Todo rechazo se debe ingresar automáticamente a la tabla de alertas. 
No puede haber ninguna demora para ingresar un rechazo en la tabla de alertas, se debe ingresar en el mismo instante en que se generó el rechazo.− 
Si una tarjeta registra dos compras en un lapso menor de un minuto en comercios distintos ubicados en el mismo código postal.− 
Si una tarjeta registra dos compras en un lapso menor de5minutos en comercios con diferentes códigos postales.− 
Si una tarjeta registra dos rechazos por exceso de límite en el mismo día, la tarjeta tiene que ser suspendida preventivamente, 
y se debe grabar una alerta asociada a este cambio de estado.
*/

create function alertarRechazo() returns trigger as $$
	DECLARE
		cant int;
		numAlerta int;
		estadoTarjeta text;
        nRechazo int;
	BEGIN
		
		-- determina el código incremental de la tabla alerta
		if exists(select * from alerta)  THEN
            numAlerta:=(select max(a.nroalerta) from alerta a);
            numAlerta:=numAlerta+1;
        ELSE
            numAlerta:=(select 1);
         end if;

		if exists(select * from rechazo)  THEN
            nRechazo:=(select max(r.nrorechazo) from rechazo r);
            nRechazo:=nRechazo+1;
        ELSE
            nRechazo:=(select 1);
         end if;
		
		insert into alerta values (numAlerta, new.nrotarjeta, new.fecha, new.nrorechazo, 0, new.motivo);

		if new.motivo = 'supera límite de tarjeta' then
			select count(*) into cant from rechazo 
			where nrotarjeta = new.nrotarjeta and motivo = new.motivo and fecha = new.fecha;
		end if;
		
		if cant > 1 then 
			insert into alerta values (numAlerta+1, new.nrotarjeta, new.fecha, new.nrorechazo, 32, new.motivo);
			select tr.estado from tarjetas tr where nrotarjeta=new.nrotarjeta;
			estadoTarjeta='suspendida';
		end if;
		return new;
end;
$$language plpgsql;

--CREACION DE LOS TRIGGERS
CREATE TRIGGER alerta_compra AFTER INSERT ON compra
    FOR EACH ROW EXECUTE PROCEDURE alertarCompra();

CREATE TRIGGER alerta_rechazo AFTER INSERT ON rechazo
    FOR EACH ROW EXECUTE PROCEDURE alertarRechazo();



