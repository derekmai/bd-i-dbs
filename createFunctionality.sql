﻿--raise notice de variables:  raise notice '"%"',  variable;
--comparar fechas en formato ddmmyy? .
--como decido 'tarjeta no válida ó no vigente'
--se ingresan dos rechazos en el caso '4685957432651425',2,'201114','201120','957','16000','suspendida'



\c tp2


DROP FUNCTION
if exists autorizarCompra(numTarjeta char(16),cod char(4),numComercio int,montoCompra decimal(7,2));
--------PRIMERA FUNCION PRIMERA CONDICION---------
--Debo insertar más de un rechazo o uno solo y salir

--'4958765842515263',8,'180912','180917','958','25000','anulada';
create function autorizarCompra(numTarjeta char(16),cod char(4),numComercio int,montoCompra decimal(7,2) ) returns boolean as $$
    declare 
        maxRechazo int;
        montoTotal int;
        limite int;
        hoy date;
        maxCompra int;
    BEGIN


    if not exists ( select * from tarjeta where nrotarjeta =numTarjeta and estado='vigente') then
        if exists(select * from rechazo)THEN
            maxRechazo:=(select max(nrorechazo) from rechazo);
            maxRechazo:=maxRechazo+1;
        else
            maxRechazo:=(select 1);
        end if;

        insert into rechazo(nrorechazo,nrotarjeta,nrocomercio,fecha,monto,motivo)
        select maxRechazo,numTarjeta,numComercio,now(),montoCompra,'tarjeta no válida ó no vigente';
         RAISE NOTICE 'tarjeta no valida o no vigente';
        return false;   
    end if;

    if not exists( select * from tarjeta where codseguridad = cod) THEN
        if exists(select * from rechazo)THEN
            maxRechazo:=(select max(nrorechazo) from rechazo);
            maxRechazo:=maxRechazo+1;
        else
            maxRechazo:=(select 1);
        end if;

        insert into rechazo(nrorechazo,nrotarjeta,nrocomercio,fecha,monto,motivo)
        select maxRechazo, numTarjeta, numComercio, now(), montoCompra, 'codigo de seguridad inválido';
        RAISE NOTICE 'codigo de seguridad invalido';
        return false; 
    end if;

    if exists (select c.monto,t.limitecompra from compra c join tarjeta t 
    on c.nrotarjeta = t.nrotarjeta where c.nrotarjeta = numTarjeta  )
    or exists(select * from tarjeta where nrotarjeta = numTarjeta and montoCompra > limitecompra)  THEN
        montoTotal:= (select sum(monto) from compra where nrotarjeta=numTarjeta and pagado=false);
        limite := (select limitecompra from tarjeta where nrotarjeta=numTarjeta);

        if ((montoTotal + montoCompra) > limite) THEN
            if exists(select * from rechazo)THEN
                maxRechazo:=(select max(nrorechazo) from rechazo);
                maxRechazo:=maxRechazo+1;
            else
                maxRechazo:=(select 1);
            end if;

            insert into rechazo(nrorechazo,nrotarjeta,nrocomercio,fecha,monto,motivo)
            select maxRechazo, numTarjeta, numComercio, now(), montoCompra, 'supera límite de tarjeta';
             RAISE NOTICE 'supera limite de tarjeta';
            return false;

        end if;
    end if;

    hoy:=(select to_date(to_char(now(),'ddmmyy'),'ddmmyy'));
    if exists(select * from tarjeta where nrotarjeta = numTarjeta and to_date(validahasta,'ddmmyy') < hoy)THEN
        if exists(select * from rechazo)THEN
            maxRechazo:=(select max(nrorechazo) from rechazo);
            maxRechazo:=maxRechazo+1;
        else
            maxRechazo:=(select 1);
        end if;

        insert into rechazo(nrorechazo,nrotarjeta,nrocomercio,fecha,monto,motivo)
        select maxRechazo, numTarjeta, numComercio, now(), montoCompra, 'plazo de vigencia expirado';  
         RAISE NOTICE 'plazo de vigencia expirado';      
        return false;
    end if;

    if exists(select * from tarjeta where nrotarjeta = numTarjeta and estado = 'suspendida')THEN
        if exists(select * from rechazo)THEN
           maxRechazo:=(select max(nrorechazo) from rechazo);
           maxRechazo:=maxRechazo+1;
        else
            maxRechazo:=(select 1);
        end if;

        insert into rechazo(nrorechazo,nrotarjeta,nrocomercio,fecha,monto,motivo)
        select maxRechazo, numTarjeta, numComercio, now(), montoCompra, 'la tarjeta se encuentra suspendida'; 
         RAISE NOTICE 'la tarjeta se encuentra suspendida';       
    	return false;
    end if;

   --INSERTO COMPRA

        if exists(select * from compra)THEN
            maxCompra:=(select max(nrooperacion) from compra);
            maxCompra:=maxCompra+1;
        else
            maxCompra:=(select 1);
        end if;

        insert into compra(nrooperacion,nrotarjeta,nrocomercio,fecha,monto,pagado)
        select maxCompra,numTarjeta,numComercio,now(),montoCompra,true ;
        raise notice 'COMPRA AUTORIZADA';
        return true;

    end;
    $$ language plpgsql;
  
/*
create 
table compra(nrooperacion
int,nrotarjeta
char(16),nrocomercio
int,fecha 
timestamp,monto 
decimal(7,2),pagado
boolean);

create 
table tarjeta(nrotarjeta
char(16),nrocliente
int,validadesde
char(6),validahasta
char(6),codseguridad
char(4),limitecompra
decimal(8,2),estado
char(10));

create 
table compra(nrooperacion
int,nrotarjeta
char(16),nrocomercio
int,fecha 
timestamp,monto 
decimal(7,2),pagado
boolean);
*/
