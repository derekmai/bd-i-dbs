# BD I - dbs

BD I (Base de Datos I) - Trabajo sobre manejo de DBs (queries, functions y triggers). Además, desarrollo de una pequeña CLI en GO para realizar algunas funcines y  queries a una db no relacional.

## Setup

Como hacer que ande el CLI:
1- hacer cd hasta donde esta el .go
2- hacer go run cli.go (para ver que cosas les falta bajar del go)
3- setear el GOPATH$ (https://github.com/golang/go/wiki/SettingGOPATH)
4- go install
5- si corren cli sin ningun comando muestra todos los comandos.

## Descripción del TP

La consigna consiste en diseñar los stored procedures,triggers y una interfaz CLI para otorgarle funcionalidad a la base de datos brindada, que opera con tarjetas de credito.


## Creacion de base de datos

Se deberán crear las tablas respetando los nombres de tablas, atributos y tipos de datos
especificados.
Se deberán agregar las PK’s y FK’s de todas las tablas, por separado de la creación de las
tablas. Además, le usuarie deberá tener la posibilidad de borrar todas las PK’s y FK’s, si
lo desea.

## Instancia de datos

Se deberán cargar 20 clientes y 20 comercios. Todes les clientes tendrán una tarjeta,
excepto dos clientes que tendrán dos tarjetas cada une. Una tarjeta deberá estar expirada
en su fecha de vencimiento.
La tabla cierre deberá tener los cierres de las tarjetas para todo el año 2018.

Se esperan los siguientes stored procedures y triggers

a. Autorizacion de compra.
b. Generacion del resumen.
c. Alertas a clientes.


## Descripción
### Autorización de compra

Se verifica que al recibir un número de tarjeta,codigo de seguridad,número de comercio y monto, devuelva true si se autoriza la compra o false si es rechazada.

Se debera insertar rechazos en las siguientes condiciones:
a. Si el numero de tarjeta no es existente o no corresponde a una tarjeta vigente( Mensaje:*tarjeta no valida o no vigente*).
b. Si el codigo de seguridad no es correcto(Mensaje: *codigo de seguridad invalido*).
c. Si el monto total de compras pendientes de pago mas la compra a realizar supera el limite de la tarjeta(Mensaje: *supera limite de tarjeta*).
d. Si la tarjeta esta vencida(Mensaje: *plazo de vigencia expirado*).
e. Si la tarjeta esta suspendida(Mensaje: *la tarjeta se encuentra suspendida*).

### Generación del resumen

Se deben guardar los datos de cada compra para poder armar un resumen con los datos de las mismas,del cliente y de la tarjeta.

Se genera la cabecera en una primera instancia, con todos los datos del cliente y uno por cada tarjeta.Luego se genera el detalle de cada resumen linea por linea de cada compra, tambien por cada tarjeta.


### Alertas a clientes

Se deben generar alertas por posibles fraudes , asi es posible que un call center pueda comunicarse con los clientes.
Las alertas se generan si una tarjeta registra dos compras en un mismo comercio en un lapso menor de un minuto,si se registran dos compras en un lapso menor de 5 minutos en comercios diferentes y si una tarjeta tiene dos rechazos por exceso de limite en el mismo dia(en ese caso se suspendera la tarjeta preventivamente).

Se insertara una alerta en los siguientes casos:

a. Todo rechazo se ingresa automaticamente a las alertas.
b. Si una tarjeta registra dos compras en un lapso menor de un minuto en comercios distintos con mismo codigo postal.
c. Si una tarjeta registra dos compras en un lapso menor de 5 minutos en comercios distintos con diferente codigo postal.
d. Si una tarjeta registra dos rechazos por exceso de limite en el mismo dia.



JSON y Bases de datos NoSQL

Por ultimo para poder comparar el modelo relacional con uno no relacional NoSQL, se pide guardar los datos de clientes,tarjetas,comercios y compras(tres por cada entidad) en una base de datos NoSQL basada en JSON.Este codigo debera ejecutarse desde una aplicacion CLI escrita en Go.
